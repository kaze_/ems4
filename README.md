# ems4

- 目錄結構

```bash
    src
    ├── gateway # 放在 gateway 執行的程式
    │   ├── realtime_data_collector.py # 收集需要即時顯示的資訊
    │   └── run_client.sh # 執行 gateway 端的 flask
    ├── server # 放在 Azure 上執行的程式
    │   ├── realtime_data_handler.py # 把每個 gateway id 對應到的 host 寫到 redis 以供 APP 使用
    │   └── run_flask.sh # 執行 server 端的 flask
    └── utils
        ├── date.py
        ├── __init__.py
        ├── logger.py
        └── parser.py
```

- 
