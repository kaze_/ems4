# /bin/bash

proto_dir="./"
python_out="./"
grpc_python_out="./"
proto_full_name="xmight.proto"
proto_name=$(echo $proto_full_name | cut -f 1 -d '.')

rm $python_out"$proto_name"_pb2_grpc.*
rm $python_out"$proto_name"_pb2.*
python3 -m grpc_tools.protoc -I "$proto_dir" --python_out="$python_out" --grpc_python_out="$grpc_python_out" "$proto_dir/$proto_full_name"

python_out=${python_out%/}
py3clean $python_out
python3 -m py_compile $python_out/"$proto_name"_pb2_grpc.py $python_out/"$proto_name"_pb2.py

mv $python_out/__pycache__/"$proto_name"_pb2_grpc*.pyc $python_out/"$proto_name"_pb2_grpc.pyc
mv $python_out/__pycache__/"$proto_name"_pb2*.pyc $python_out/"$proto_name"_pb2.pyc
