import errno
import fcntl
import os
import signal
import time
from multiprocessing import Process
from queue import Queue

from daemon import DaemonContext
from psutil import pid_exists
from TWCManager.twcmanager import TWCManager
from utils.logger import Logger
from utils.scheduler import Scheduler

logger = Logger('gateway_daemon.log', level=Logger.DEBUG).get_logger()


# TODO 加入記憶體及 CPU 用量的紀錄及警示
class GatewayDaemon(object):
    """
    管理所有在 gateway 上執行的程式
    """
    __running_processes = []

    def start(self):
        logger.info('-------------Start Gateway Daemon-------------')
        self.lanuch_twcmanager()
        time.sleep(1)

        scheduler = Scheduler(logger)
        # TODO 檢查的間隔可能要再調
        event = (10, self.monitor_processes, ())
        scheduler.add_event(event)
        scheduler.run()

    def monitor_processes(self):
        if not self.__running_processes:
            logger.error('not any process running')
            return

        for process_info in self.__running_processes:
            process, command_queue = process_info.items()
            # TODO 每個 process 是否需要重啟的記數，到一定值再回報
            if not pid_exists(process.pid) or not process.is_alive():
                logger.error('process "%s" was be terminated, try to restart it', process.name)
                self.lanuch_process(process.name)

    def lanuch_twcmanager(self):
        # twcmanager 允許每個 port 一個實體，所以這裡不用 pid file 做檢查
        twcmanager = TWCManager()
        command_queue = Queue()
        twcmanager_process = Process(target=twcmanager.start, args=(command_queue,), name='twcmanager')
        process_info = {'process': twcmanager_process, 'command_queue': command_queue}
        self.__running_processes.append(process_info)
        twcmanager_process.start()

    def lanuch_process(self, process_name: str):
        """
        啟動特定的 process

        Args:
            process_name (str): 程序的名稱

        Raises:
            ValueError: 當程序名稱為空，或不存在時拋出
        """
        if not process_name or process_name not in ['twcmanager']:
            raise ValueError

        if process_name == 'twcmanager':
            self.lanuch_twcmanager()
        elif process_name == '':
            pass


class PidFile(object):
    """
    提供給 python-daemon 的 pidfile 參數使用
    """
    __pid_file_path = ''
    __pid_file = None

    def __init__(self, pid_file_path: str):
        self.__pid_file_path = pid_file_path

    def __enter__(self):
        self.__pid_file = open(self.__pid_file_path, 'w')
        try:
            fcntl.flock(self.__pid_file.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            logger.error('gateway daemon already running, pid: %s', self.__get_old_pid(self.__pid_file_path))
            raise SystemExit('The gateway daemon already running')

        pid = str(os.getpid())
        self.__pid_file.write(pid)
        self.__pid_file.flush()
        return self.__pid_file

    def __get_old_pid(self, path: str) -> str:
        with open(path, 'r') as old_pid_file:
            return old_pid_file.read()

    def __exit__(self, type, value, traceback):
        try:
            self.__pid_file.close()
        except IOError as err:
            logger.exception('can not close pid file, error: %s', err)
        os.remove(self.__pid_file_path)


def kill_zombie_process(signal_number: int, frame):
    """
    防止產生殭屍行程

    Args:
        signal_number (int): 收到的訊號
        frame (frame): frame object
    """
    while True:
        try:
            pid, exit_status_indication = os.waitpid(-1, os.WNOHANG)
        except OSError as err:
            if err.errno == errno.ECHILD:
                logger.warning('no child process')
                break
            else:
                raise

        if pid == 0:
            break
        logger.info('process %s exited, exit code %s', pid, exit_status_indication >> 8)


if __name__ == '__main__':
    # TODO 以後從設定檔讀
    working_directory = '/home/xmight/Desktop/test/twcmanager/'
    signal_map = {signal.SIGTSTP: None, signal.SIGTTIN: None, signal.SIGTTOU: None,
                  signal.SIGTERM: 'terminate', signal.SIGCHLD: kill_zombie_process}

    # gateway daemon 的 pidfile，一次只允許一個 gateway daemon 實體
    pidfile = PidFile('/var/run/gateway_daemon.pid')

    # 透過 logger 的 FileHandlers 拿到 _io.TextIOWrapper 物件，用於保留所有 log 輸出，DaemonContext 會自己嘗試拿到 fileno
    loggers_stream = list(map(lambda handler: handler.stream, Logger.get_file_handlers()))

    # TODO 設定檔如果調成 debug mode，這裡要改成 stdout=sys.stdout 來顯示輸出
    stdout = None

    gateway_daemon = GatewayDaemon()
    with DaemonContext(working_directory=working_directory, signal_map=signal_map, pidfile=pidfile, files_preserve=loggers_stream, stdout=stdout):
        gateway_daemon.start()
