import sys
import unittest

sys.path.append('../../TWCManager')
from communicator import Communicator
from slave import Slave
from slaves_manager import SlavesManager
from utils.logger import Logger

logger = Logger('test_communicator.log', level=Logger.DEBUG).get_logger()


class TestCommunicator(unittest.TestCase):
    """
    測試 communicator.py
    """
    __communicator = None
    __slaves_manager = None
    __slave_id = b'\x66'

    @classmethod
    def setUpClass(cls):
        test_slave = Slave()
        test_slave.update({'slave_id': cls.__slave_id, 'vin_code': bytearray()})
        cls.__slaves_manager = SlavesManager()
        cls.__slaves_manager.add_slave(test_slave)
        cls.__communicator = Communicator(None, cls.__slaves_manager, logger)

    def test_receive_vin_code(self):
        fake_vin_codes_data = [{'order': b'\xee', 'vin_code': b'\x35\x59\x4A\x33\x45\x37\x45'}, {'order': b'\xef', 'vin_code': b'\x42\x30\x4C\x46\x36\x30\x35'}, {'order': b'\xf1', 'vin_code': b'\x33\x31\x39\x00\x00\x00\x00'}]
        for vin_code_data in fake_vin_codes_data:
            vin_code = vin_code_data.get('vin_code')
            order = vin_code_data.get('order')
            self.__communicator.receive_vin_code(self.__slave_id, vin_code, order)

        vin_code_string = '5YJ3E7EB0LF605319'
        slave_vin_code = self.__slaves_manager.get_vin_code(self.__slave_id)
        self.assertEqual(vin_code_string, slave_vin_code, 'slave_vin_code: {}'.format(slave_vin_code))


if __name__ == "__main__":
    unittest.main()
