import json
import sys
import time
import unittest

sys.path.append('../../TWCManager')
from slave import Slave
from slaves_manager import SlavesManager


class TestSlavesManager(unittest.TestCase):
    """
    測試 communicator.py
    """
    __slaves_manager = None
    __fake_slave = None
    __slave_id = b'\x66'

    def setUp(self):
        test_slave = Slave()
        test_slave.update({'slave_id': self.__slave_id})
        self.__fake_slave = test_slave

        self.__slaves_manager = SlavesManager()
        self.__slaves_manager.add_slave(self.__fake_slave)

    def test_add_slave(self):
        self.__slaves_manager.add_slave(self.__fake_slave)
        current_slaves = self.__slaves_manager.get_slaves()
        self.assertEqual([self.__fake_slave], current_slaves, 'current slaves: {}'.format(current_slaves))

    def test_get_slave(self):
        self.__slaves_manager.add_slave(self.__fake_slave)
        current_slave = self.__slaves_manager.get_slave(self.__slave_id)
        self.assertEqual(self.__fake_slave, current_slave, 'current slave: {}'.format(current_slave))

    def test_set_heartbeat_data(self):
        fake_heartbeat_data = bytearray(b'\x09\x04\xB0\x00\x00\x00\x00\x00\x00')
        self.__slaves_manager.set_heartbeat_data(self.__slave_id, fake_heartbeat_data)

        current_heartbeat_data = self.__slaves_manager.get_slave(self.__slave_id).get('heartbeat_data')
        self.assertEqual(fake_heartbeat_data, current_heartbeat_data,
                         'current heartbeat data: {}'.format(current_heartbeat_data))

    def test_set_response_time(self):
        now_time = time.time()
        fake_response_time = now_time
        self.__slaves_manager.set_response_time(self.__slave_id, fake_response_time)

        current_response_time = self.__slaves_manager.get_slave(self.__slave_id).get('last_response_time')
        self.assertEqual(fake_response_time, current_response_time,
                         'current response time: {}'.format(current_response_time))

    def test_update_recorder(self):
        fake_recorde = {'Now Plug': 'P'}
        self.__slaves_manager.update_recorder(self.__slave_id, fake_recorde)

        current_recorder = self.__slaves_manager.get_recorder(self.__slave_id)
        current_recorder = json.loads(current_recorder).get('Now Plug')
        self.assertEqual(fake_recorde.get('Now Plug'), current_recorder,
                         'current recorder: {}'.format(current_recorder))

    def test_clean_vin_code(self):
        fake_vin_code = bytearray(b'\x35\x59\x4A\x33\x45\x37\x45\x42\x30\x4C\x46\x36\x30\x35\x33\x31\x39')
        vin_code_string = '5YJ3E7EB0LF605319'
        self.__slaves_manager.set_vin_code(self.__slave_id, fake_vin_code)
        current_vin_code = self.__slaves_manager.get_vin_code(self.__slave_id)
        self.assertEqual(vin_code_string, current_vin_code, 'current vin code: {}'.format(current_vin_code))

        self.__slaves_manager.clean_vin_code(self.__slave_id)
        current_vin_code = self.__slaves_manager.get_vin_code(self.__slave_id)
        self.assertEqual('', current_vin_code, 'current vin code: {}'.format(current_vin_code))

    def test_is_duplicate_slave_id(self):
        result = self.__slaves_manager._SlavesManager__is_duplicate_slave_id(self.__slave_id)
        self.assertEqual(True, result)

    def test_update_slave_info(self):
        fake_info = {'last_response_time': time.time()}
        self.__slaves_manager._SlavesManager__update_slave_info(self.__slave_id, fake_info)

        slave = self.__slaves_manager.get_slave(self.__slave_id)
        current_last_response_time = slave.get('last_response_time')
        self.assertEqual(fake_info.get('last_response_time'), current_last_response_time,
                         'current last response time: {}'.format(current_last_response_time))


if __name__ == "__main__":
    unittest.main()
