#! /usr/bin/python3
# coding=utf-8

import socketio

API_URL = 'http://118.163.133.56:8000/'
# API_URL = 'http://127.0.0.1:5005/'

GATEWAY_ID = '2e59c46a-924c-4ab1-a7ff-8d0226402179'
# GATEWAY_ID = '742bbbf1-cc32-4b47-9f5c-4128438b6391'
client = socketio.Client()


class ClientEmulator(socketio.ClientNamespace):
    count = 0

    def __init__(self, *args, **kwargs):
        super(ClientEmulator, self).__init__(*args, **kwargs)

    def on_connect(self):
        self.emit('require_realtime_data', GATEWAY_ID)

    def on_disconnect(self):
        print('disconnect')

    def on_receive_realtime_data(self, data: str):
        print('data: {}'.format(data))
        self.count += 1
        if self.count >= 5:
            self.stop_receive_realtime_data(GATEWAY_ID)

    def stop_receive_realtime_data(self, gateway_id):
        self.emit('require_stop_realtime_data', gateway_id)


if __name__ == "__main__":
    client.register_namespace(ClientEmulator())
    client.connect(API_URL)
    client.wait()
