import fcntl
import re
import threading
import time
from queue import Queue

import serial

from TWCManager.communicator import Communicator
from utils import get_hex_string, is_thread_alive
from utils.logger import Logger
from utils.scheduler import Scheduler

logger = Logger('twcmanager.log', level=Logger.DEBUG).get_logger()


class TWCManager(object):
    """
    與特斯拉充電樁溝通的邏輯
    """
    __serial_handler = None
    __communicator = None
    __timing = 0
    __messages = Queue()
    # TODO 以後從設定檔讀取
    __set_current = 12

    def __init__(self, serial_port='/dev/ttymxc4', baudrate=9600):
        self.__serial_handler = serial.Serial(serial_port, baudrate, timeout=1, write_timeout=1)
        try:
            fcntl.flock(self.__serial_handler.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError:
            logger.error('target port %s is busy', serial_port)
            raise
        self.__serial_handler.reset_input_buffer()
        self.__serial_handler.reset_output_buffer()
        self.__communicator = Communicator(self.__serial_handler, logger)

    def start(self, commands_queue: Queue = None):
        """
        開始對特斯拉充電樁進行管理

        Args:
            messages_queue (Queue, optional): 對 twcmanager 下的指令
        """
        logger.info('-------------Start TWCManager-------------')
        # receiver 要比 sender 慢啟動
        self.start_sender()
        self.start_receiver()

        while True:
            time.sleep(1)
            if commands_queue and not commands_queue.empty():
                command = commands_queue.get()
                logger.info('get command: %s', command)

    def start_sender(self):
        """
        啟動發送命令的 thread
        """
        if not is_thread_alive('sender'):
            scheduler = Scheduler(logger)
            event = (1, self.__send_command, ())
            scheduler.add_event(event)
            sender = threading.Thread(target=scheduler.run, name='sender')
            logger.info('start sender thread')
            sender.start()

    def start_receiver(self):
        """
        啟動接收回傳的 thread
        """
        time.sleep(0.5)
        if not is_thread_alive('receiver'):
            receiver = threading.Thread(target=self.__receive_data, name='receiver')
            logger.info('start receiver thread')
            receiver.start()

    def read_config(self):
        # TODO 從設定檔讀取最大電流，以及要設定的電流
        set_current = 42
        max_current = 32
        # TODO 設定超過最大電流的值，則以最大電流為主
        if set_current >= max_current:
            self.__set_current = max_current
        else:
            self.__set_current = set_current

    def notify_server(self):
        # TODO 通知 server 特定的事件，e.g. charger offline, not validated user etc.
        pass

    def reported_offline_slaves(self):
        now_time = time.time()
        for slave in self.__communicator.get_slaves():
            if now_time - slave.get('last_response_time') >= 30:
                # TODO 通知 server 哪個 slave offline
                self.notify_server()

    def __send_command(self):
        """
        依據狀態依序送出命令
        """
        if not self.__communicator.get_slaves():
            self.__communicator.send_master_check()
            self.__communicator.send_request_slave_id()
            return

        if self.__timing == 0 or self.__timing == 2:
            self.__communicator.send_heartbeat()
        elif self.__timing == 1:
            self.__communicator.query_plug_status()
        elif self.__timing == 3:
            self.__communicator.query_power_info()

        self.__timing += 1
        if self.__timing > 3:
            self.__timing = 0

    def __receive_data(self):
        """
        接收特斯拉充電樁回傳的訊息
        """
        response_data = bytearray()
        last_received_time = time.time()
        while True:
            time.sleep(0.2)
            data_length = self.__serial_handler.in_waiting
            if not data_length and time.time() - last_received_time >= 30:
                logger.warning('can not read any messages from slaves')

            try:
                received_data = self.__serial_handler.read(data_length)
            except serial.SerialException as err:
                logger.exception('can not read data from serial port, error: %s', err)
                continue
            response_data += received_data

            end_index = response_data.find(b'\xC0')
            if end_index == -1:
                logger.debug('can not found C0 byte, response data: %s', response_data)
                continue

            message = response_data[:end_index + 1]
            self.__messages.put(message)
            last_received_time = time.time()
            logger.info('RX: %s', get_hex_string(message))
            response_data = response_data[end_index + 1:]
            self.__process_messages()

    def __process_messages(self):
        """
        對收到的命令做解析，並呼叫對應的處理函數
        """
        while not self.__messages.empty():
            response_data = self.__messages.get()
            response_data = self.__get_unpacked_message(response_data)
            if len(response_data) >= 14 and self.__validate_message(response_data):
                self.__match_message(response_data)
            self.__messages.task_done()

    def __match_message(self, response_data: bytearray):
        """
        使用 regex 來解析回應的訊息，並呼叫對應的處理方法

        Args:
            response_data (bytearray): 要解析的訊息
        """
        match_data = re.search(b'^\xfd\xe2(..)(.)(..)\x00{6}.+\Z', response_data, re.DOTALL)
        if match_data and len(self.__communicator.get_slaves()) < 3:
            logger.info('-------------receive_slave_id-------------')
            slave_id = match_data.group(1)
            self.__communicator.receive_slave_id(slave_id)
            return

        match_data = re.search(b'\A\xfd\xe0(..)(..)(.......+?).\Z', response_data, re.DOTALL)
        if match_data:
            logger.info('-------------receive_heartbeat-------------')
            slave_id = match_data.group(1)
            heartbeat_data = match_data.group(3)
            if self.__communicator.is_slave_id_exist(slave_id):
                self.__communicator.receive_heartbeat(slave_id, heartbeat_data)
            return

        match_data = re.search(b'\A\xfd\xeb(..)(....)(..)(..)(..)(.)(.)(.)...\Z', response_data, re.DOTALL)
        if match_data:
            logger.info('-------------receive_power_info-------------')
            slave_id = match_data.group(1)
            power_info = {'Acc Power': match_data.group(2),
                          'V1': match_data.group(3),
                          'V2': match_data.group(4),
                          'V3': match_data.group(5),
                          'I1': match_data.group(6),
                          'I2': match_data.group(7),
                          'I3': match_data.group(8)}
            if self.__communicator.is_slave_id_exist(slave_id):
                self.__communicator.receive_power_info(slave_id, power_info)
            return

        match_data = re.search(b'\A\xfd\xb4(..)(.)\x00{10}.\Z', response_data, re.DOTALL)
        if match_data:
            logger.info('-------------receive_plug_status-------------')
            slave_id = match_data.group(1)
            plug_status = match_data.group(2)
            if self.__communicator.is_slave_id_exist(slave_id):
                self.__communicator.receive_plug_status(slave_id, plug_status)
            return

        match_data = re.search(b'\A\xfd(\xee|\xef|\xf1)(..)(.......)\x00{8}.\Z', response_data, re.DOTALL)
        if match_data:
            logger.info('-------------receive_vin_code-------------')
            order = match_data.group(1)
            slave_id = match_data.group(2)
            vin_code = match_data.group(3)
            if self.__communicator.is_slave_id_exist(slave_id):
                self.__communicator.receive_vin_code(slave_id, vin_code, order)
            return

        match_data = re.search(b'\A\xfc(\xe1|\xe2)(..)(.)\x00{8}.+\Z', response_data, re.DOTALL)
        if match_data:
            logger.info('-------------receive_multiple_master_error-------------')
            logger.error('found another master, try to change fake master id')
            self.__communicator.update_fake_master_id()
            return

        logger.error('unknown message: %s', get_hex_string(response_data))

    def __get_unpacked_message(self, message: bytearray) -> bytearray:
        """
        還原保留字，以及移除 0xC0 byte

        Args:
            message (bytearray): 要處理的資料
            message_length (int): 資料的合法長度，不等於資料長度

        Returns:
            bytearray: 處理好的資料
        """
        # 移除 0xC0 byte
        identify_byte_index = message.find(b'\xC0')
        message = message[:identify_byte_index]

        # 還原保留字
        for i in range(len(message) - 2):
            if message[i] == 0xdb:
                if message[i + 1] == 0xdc:
                    message[i: i + 2] = [0xc0]
                elif message[i + 1] == 0xdd:
                    message[i: i + 2] = [0xdb]
                else:
                    unlegal_char = message[i + 1]
                    logger.error('escape character 0xDB followed by invalid character: %s',
                                 get_hex_string(unlegal_char))
                    message[i: i + 2] = [0xdb]
        return message

    def __validate_message(self, message: bytearray) -> bool:
        """
        驗證是否為合法的訊息

        Args:
            message (bytearray): 要驗證的訊息

        Returns:
            bool: 驗證成功回傳 True，反之回傳 False
        """
        if not message:
            return False

        expected_checksum = message[len(message) - 1]
        checksum = 0
        for i in range(1, len(message) - 1):
            checksum += message[i]
        checksum = checksum & 0xFF

        if(checksum != expected_checksum):
            logger.error('checksum of message: %s does not match', get_hex_string(message))
            return False
        return True


if __name__ == '__main__':
    twcmanager = TWCManager()
    twcmanager.start()
