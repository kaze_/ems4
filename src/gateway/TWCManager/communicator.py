import random
import time
from logging import RootLogger

import serial

from TWCManager.recorder import Recorder
from TWCManager.slave import Slave
from TWCManager.slaves_manager import SlavesManager
from utils import get_hex_string
from utils.date import get_isoformat_datetime


class Communicator(object):
    """
    處理傳送及接收特斯拉充電樁支援的命令
    """
    BLANK_DATA = bytearray(9)
    __logger = None
    __serial_handler = None
    __fake_master_id = bytearray(b'\x77\x77')
    __fake_chk_number = bytearray(b'\x77')
    __slaves = SlavesManager()
    # TODO 以後從設定檔讀取
    __set_current = 12

    def __init__(self, serial_handler: serial.Serial, logger: RootLogger):
        self.__serial_handler = serial_handler
        self.__logger = logger

    def send_master_check(self):
        """
        送出確認有無其他 master 的命令
        """
        self.__logger.info('send master check command to all slaves')
        command = bytearray(b'\xFC\xE1')
        self.__send_msg(command + self.__fake_master_id + self.__fake_chk_number + self.BLANK_DATA)

    def send_request_slave_id(self):
        """
        送出請求所有 slaves id 的命令
        """
        self.__logger.info('send request slave id command to all slaves')
        command = bytearray(b'\xFB\xE2')
        self.__send_msg(command + self.__fake_master_id + self.__fake_chk_number + self.BLANK_DATA)

    def send_heartbeat(self):
        """
        用於 heartbeat，同時也是對特斯拉充電樁下命令的時機
        """
        self.__logger.info('-------------send_heartbeat-------------')
        if not self.__slaves:
            self.__logger.error('can not found any slaves')
            return

        command = bytearray(b'\xFB\xE0')
        for slave in self.__slaves:
            slave_id = slave.get('slave_id')
            heartbeat_data = slave.get('heartbeat_data')
            self.__send_msg(command + self.__fake_master_id + slave_id + heartbeat_data)

    def query_plug_status(self):
        """
        送出查詢充電槍狀態的請求
        """
        self.__logger.info('-------------query_plug_status-------------')
        command = bytearray(b'\xFB\xB4')
        self.__send_command_to_all(command, self.BLANK_DATA)

    def query_power_info(self):
        """
        發送查詢充電樁電力資訊的請求
        """
        self.__logger.info('-------------query_power_info-------------')
        command = bytearray(b'\xFB\xEB')
        self.__send_command_to_all(command, self.BLANK_DATA)

    def query_vin_code(self, slave_id: bytes):
        """
        發送查詢 VIN Code 的請求，因為 VIN Code 很長，需要三個命令
        """
        self.__logger.info('-------------query_vin_code-------------')
        commands = [bytearray(b'\xFB\xEE'), bytearray(b'\xFB\xEF'), bytearray(b'\xFB\xF1')]
        for command in commands:
            self.__send_msg(command + self.__fake_master_id + slave_id + self.BLANK_DATA)

    def update_fake_master_id(self, fake_master_id=bytearray()):
        """
        更新 fake master id
        """
        new_fake_master_id = None
        if fake_master_id:
            new_fake_master_id = fake_master_id
        else:
            random.seed(time.time() * 1.8)
            random_ascii = random.sample(range(1, 118), 2)
            new_fake_master_id = bytearray(random_ascii)

        self.__logger.info('update fake master id to %s', get_hex_string(new_fake_master_id))
        self.__fake_master_id = new_fake_master_id

    def receive_slave_id(self, slave_id: bytes):
        """
        初始化 Slave 物件，並加到 list 裡

        Args:
            slave_id (bytes): 收到的 slave id
        """
        new_slave = Slave()
        # TODO charger_id 與 slave_id 的關係看是要在 server 處理，還是這邊建立完再回報 server
        # TODO 之後使用 charger_id 來當成 recorder 的檔名
        recorde_path = './' + get_hex_string(slave_id).replace(' ', '') + 'charger_status.json'
        new_slave.update({'slave_id': slave_id, 'charger_id': '', 'heartbeat_data': self.BLANK_DATA,
                          'last_response_time': time.time(), 'status_recorde': Recorder(recorde_path)})
        self.__slaves.add_slave(new_slave)

    def receive_heartbeat(self, slave_id: bytes, heartbeat_data: bytes):
        """
        解析該 slave 回傳的充電樁狀態 (hearbeat data)，並依據設定檔來決定是否調整最大電流

        Args:
            slave_id (bytes): 收到的 slave id
            heartbeat_data (bytes): 收到的 hearbeat data
        """
        now_time = time.time()
        self.__slaves.update_last_received_time(slave_id, now_time)
        slave_status = heartbeat_data[0]
        if slave_status == 2:
            # TODO 可能要改成 log 充電樁的 id
            self.__logger.warning('slave %s reporte alarm', get_hex_string(slave_id))
        self.__slaves.set_response_time(slave_id, now_time)

        # TODO 需要測試是否能夠控制車子的充電電流，可以的話預設給到設定電流的大小
        slave_now_current = ((heartbeat_data[3] << 8) + heartbeat_data[4]) / 100

        slave_max_current = ((heartbeat_data[1] << 8) + heartbeat_data[2]) / 100
        new_heartbeat_data = None
        if slave_max_current != self.__set_current:
            set_current_high_byte = (self.__set_current * 100 >> 8) & 0xFF
            set_current_low_byte = (self.__set_current * 100) & 0xFF
            new_heartbeat_data = bytearray(
                [0x09, set_current_high_byte, set_current_low_byte, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
        else:
            new_heartbeat_data = self.BLANK_DATA
        self.__slaves.set_heartbeat_data(slave_id, new_heartbeat_data)

    def receive_power_info(self, slave_id: bytes, power_info: dict):
        """
        紀錄 power info

        Args:
            slave_id (bytes): 收到的 slave id
            power_info (dict): 電力資訊
        """
        now_time = time.time()
        self.__slaves.update_last_received_time(slave_id, now_time)
        power_update_time = get_isoformat_datetime(now_time)
        power_info = {key: self.__get_correct_power_info(key, value) for key, value in power_info.items()}
        if power_update_time:
            power_info.update({'Power Update Time': power_update_time})

        self.__slaves.update_recorder(slave_id, power_info)
        self.__slaves.update_last_acc_power(slave_id, power_info.get('Acc Power'))

        self.__logger.info('received slave %s power info %s', get_hex_string(slave_id), power_info)
        self.__logger.debug('recorder of slave %s: %s', get_hex_string(slave_id), self.__slaves.get_recorder(slave_id))

    def receive_plug_status(self, slave_id: bytes, plug_status: bytes):
        """
        依據回傳的充電槍狀態，紀錄相關資料到檔案
        00: unplugged
        01: charging
        02: ??, unknown
        03: plugged, but not charging

        Args:
            slave_id (bytes): 收到的 slave id
            new_plug_status (bytes): 充電槍狀態
        """
        plug_status = get_hex_string(plug_status)
        last_plug_status = self.__slaves.get_last_plug_status(slave_id)

        self.__logger.info('receive slave %s plug status: %s', get_hex_string(slave_id), plug_status)
        self.__logger.debug('slave %s last plug status: %s', get_hex_string(slave_id), last_plug_status)

        last_received_time = self.__slaves.get_last_received_time(slave_id)
        if not last_plug_status:
            # 初始化
            self.__update_plug_record(slave_id, plug_status)
        elif last_plug_status == '00' and plug_status != '00':
            # 從 00 到 01/02/03
            self.__update_plug_record(slave_id, plug_status, plug_in=last_received_time, plug_update_time=True)
        elif last_plug_status != '00' and plug_status == '00':
            # 從 01/02/03 到 00
            self.__update_plug_record(slave_id, plug_status, plug_out=last_received_time, plug_update_time=True)
            self.__slaves.clean_vin_code(slave_id)

        # 檢查是否正確拿到 vin code
        if plug_status != '00' and not self.__slaves.is_complete_vin_code(slave_id):
            self.__slaves.clean_vin_code(slave_id)
            self.query_vin_code(slave_id)

        # 在插入充電槍時，以及已經插入充電槍的狀態下，查詢車輛的 vin code
        if (plug_status != '00' and not last_plug_status) or (plug_status != '00' and last_plug_status == '00'):
            self.query_vin_code(slave_id)

    def receive_vin_code(self, slave_id: bytes, vin_code: bytes, order: bytes):
        """
        解析接收到的 vin code，依據 order 來決定是第幾段

        Args:
            slave_id (bytes): 收到的 slave id
            vin_code (bytes): 收到的 vin code
            order (bytes): 收到的命令類型，用來判斷是第幾段的 vin code
        """
        slave = self.__slaves.get_slave(slave_id)
        old_vin_code = slave.get('vin_code')

        if order == b'\xee':
            old_vin_code[0: 0] += vin_code
        elif order == b'\xef':
            old_vin_code[7: 15] += vin_code
        elif order == b'\xf1':
            old_vin_code += vin_code[:3]

        self.__logger.debug('received: vin code: %s', self.__slaves.get_vin_code(slave_id))

    def get_slaves(self) -> list:
        return self.__slaves

    def is_slave_id_exist(self, slave_id: bytes) -> bool:
        """
        查詢該 slave id 是否存在於 slaves manager 裡

        Args:
            slave_id (bytes): 要查詢的 slave id

        Returns:
            bool: 已經存在回傳 True，反之回傳 False
        """
        if not self.__slaves.get_slave(slave_id):
            return False
        return True

    def __send_command_to_all(self, command: bytearray, data_body: bytearray):
        """
        送出命令到所有的 slaves（除了 heartbeat 命令，因為每個 slave 會有各自的 heartbeat data）

        Args:
            command (bytearray): 要下的命令
            data_body (bytearray): 該命令要附帶的參數
        """
        for slave in self.__slaves:
            slave_id = slave.get('slave_id')
            self.__send_msg(command + self.__fake_master_id + slave_id + data_body)

    def __send_msg(self, message: bytearray):
        """
        發送命令到特斯拉充電樁，一個命令會送兩次

        Args:
            message (bytearray): 要送的命令

        Raises:
            ValueError: 當命令為不支援的類型時拋出
        """
        if not isinstance(message, bytearray):
            self.__logger.error('unsupported data type: %s', type(message))
            raise ValueError

        checksum = 0
        for i in range(1, len(message)):
            checksum += message[i]
        message.append(checksum & 0xFF)

        # 針對保留字做處理
        # 參考 SLIP protocol： https://en.wikipedia.org/wiki/Serial_Line_Internet_Protocol
        for i in range(0, len(message) - 1):
            if(message[i] == 0xc0):
                message[i: i + 1] = b'\xdb\xdc'
            elif(message[i] == 0xdb):
                message[i: i + 1] = b'\xdb\xdd'

        message = bytearray(b'\xc0' + message + b'\xc0')
        self.__logger.info('Tx: %s', get_hex_string(message))

        for _ in range(0, 2):
            time.sleep(0.1)
            self.__serial_handler.write(message)

    def __get_correct_power_info(self, key: str, value: bytes):
        """
        依據條件把 power info 轉成正確的表示方式
        Acc Power，V1~3: int
        I1~3：float

        Args:
            bytes_str (bytes): power info 的原始資料
            key (str): 目前處理的資料名稱

        Returns:
            int/float: 正確的數值
        """
        hex_string = get_hex_string(value).replace(' ', '')
        int_data = int(hex_string, 16)

        # 電流的回傳值是 0.5 一個單位，所以要除二
        current_preprocess = ['I1', 'I2', 'I3']
        if key in current_preprocess:
            int_data = int_data / 2

        return int_data

    def __update_plug_record(self, slave_id: bytes, plug_status='', plug_in=0.0, plug_out=0.0, plug_update_time=False):
        """
        依據條件更新 record

        Args:
            slave_id (bytes): 收到的 slave id
            plug_status (str): 目前的充電槍狀態
            plug_in (float, optional): 插上充電槍的時間
            plug_out (float, optional): 拔掉充電槍的時間
            plug_update_time (bool, optional): 是否需要更新充電槍狀態變化的時間
        """
        plug_status_mapping = {'00': 'U', '01': 'C', '03': 'P', '02': '??'}
        plug_status_code = plug_status_mapping.get(plug_status)
        if plug_status_code is None:
            self.__logger.error('cant not found related plug status code: %s', plug_status)

        self.__slaves.update_last_plug_status(slave_id, plug_status)

        if plug_in:
            # 在插入充電槍時更新最後插入的時間
            last_plug_on_time = get_isoformat_datetime(plug_in)
            new_recorde = {'Last Plug On': last_plug_on_time}
            self.__slaves.update_recorder(slave_id, new_recorde)

        if plug_out:
            # 在拔出充電槍時更新最後拔出的時間
            last_plug_off_time = get_isoformat_datetime(plug_out)
            new_recorde = {'Last Plug Off': last_plug_off_time}
            self.__slaves.update_recorder(slave_id, new_recorde)

        if plug_update_time:
            # 插入及拔出充電槍時，更新充電槍的狀態及該狀態最後更新的時間
            new_plug_update_time = get_isoformat_datetime(time.time())
            new_recorde = {'Now Plug': plug_status_code, 'Plug Update Time': new_plug_update_time}
            self.__slaves.update_recorder(slave_id, new_recorde)

        if plug_out and plug_update_time:
            # 在結束充電（拔出充電槍）時更新最後收到的 acc power
            last_acc_power = self.__slaves.get_last_acc_power(slave_id)
            new_recorde = {'Last Acc Power': last_acc_power}
            self.__slaves.update_recorder(slave_id, new_recorde)

        if plug_status and not plug_in and not plug_out and not plug_update_time:
            # 初始化
            new_recorde = {'Now Plug': plug_status_code}
            self.__slaves.update_recorder(slave_id, new_recorde)

        self.__logger.debug('recorder content: %s', self.__slaves.get_recorder(slave_id))
