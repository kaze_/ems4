import fcntl
import json
import os
from collections import UserDict


class Recorder(UserDict):
    __file_path = ''

    def __init__(self, path='./charger_status.json'):
        self.data = {'Version': 'XMTWC.2.1',
                     'TWC Last Boot': '',
                     'Last Plug On': '',
                     'Last Plug Off': '',
                     'Last Acc Power': 0.0,
                     'Plug Update Time': '',
                     'Now Plug': '',
                     'Power Update Time': '',
                     'Acc Power': 0,
                     'V1': 0, 'V2': 0, 'V3': 0, 'I1': 0.0, 'I2': 0.0, 'I3': 0.0}
        self.__file_path = path

    def __setitem__(self, key, value):
        if isinstance(value, type(self.data[key])):
            self.data[key] = value

    def update(self, new_record: dict):
        """
        更新紀錄內容，要注意值的型別需要與初始化時相同

        Args:
            new_record (dict): 新的紀錄

        Returns:
            bool: 更新成功回傳 true, 失敗回傳 false
        """
        if new_record is None:
            return False

        for key, value in new_record.items():
            self[key] = value
        return True

    def write_to_file(self):
        """
        把紀錄寫入檔案，預設是 ./charger_status.json
        """
        if not os.path.isfile(self.__file_path):
            dir_path = os.path.dirname(self.__file_path)
            file_name = os.path.basename(self.__file_path)
            open(dir_path + '/' + file_name, 'a').close()

        with open(self.__file_path, 'w') as record_file:
            fcntl.lockf(record_file, fcntl.LOCK_EX)
            record_file.write(self.get_json_string())
            fcntl.lockf(record_file, fcntl.LOCK_UN)

    def get_json_string(self) -> str:
        """
        把紀錄轉成 json 格式的字串後回傳

        Returns:
            str: json 格式的字串
        """
        return json.dumps(self.data)

    def add_field(self, new_field: dict):
        """
        新增欄位及其初始值

        Args:
            new_field (dict): 新欄位的字典，需包含名稱及初始值

        Returns:
            bool: 新增成功回傳 true，失敗回傳 false
        """
        if not isinstance(new_field, dict):
            return False

        self.data.update(new_field)
        return True

    def remove_field(self, field_name: str):
        """
        移除指定欄位及值

        Args:
            field_name (str): 要移除的欄位名稱

        Returns:
            bool: 移除成功回傳 true，失敗回傳 false
        """
        if field_name not in self.data:
            return False

        self.data.pop(field_name)
        return True
