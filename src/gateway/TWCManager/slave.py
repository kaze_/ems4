from collections import UserDict

from TWCManager.recorder import Recorder


class Slave(UserDict):
    """
    紀錄每個 slave 具有的屬性
    """

    def __init__(self):
        self.data = {'slave_id': b'', 'charger_id': '', 'heartbeat_data': bytearray(), 'last_response_time': 0.0,
                     'status_recorde': Recorder(), 'vin_code': bytearray(), 'last_plug_status': '', 'last_received_time': 0.0, 'last_acc_power': 0}

    def __setitem__(self, key, value):
        if isinstance(value, type(self.data[key])):
            self.data[key] = value
        else:
            expected_type = type(self.data[key]).__name__
            current_type = type(value).__name__
            raise ValueError('data type of "{}" value expected "{}", but "{}"'.format(key, expected_type, current_type))
