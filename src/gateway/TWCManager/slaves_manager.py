from collections import UserList

from TWCManager.slave import Slave
from utils import get_hex_string


class SlavesManager(UserList):
    """
    管理及存放 Slave
    """
    def __setitem__(self, i, item):
        if isinstance(item, type(Slave())):
            self.data[i] = item

    def append(self, item):
        if not isinstance(item, type(Slave())):
            return

        new_slave_id = item.get('slave_id')
        if not self.__is_duplicate_slave_id(new_slave_id):
            super().append(item)

    def add_slave(self, new_slave: Slave):
        """
        新增 slave，只允許新增 Slave 物件

        Args:
            new_slave (Slave): 要新增的 slave
        """
        self.append(new_slave)

    def remove_slave(self, target_slave: dict):
        # TODO 看要依據什麼來移除 slave
        pass

    def get_slaves(self) -> list:
        return self.data

    def get_slave(self, slave_id: bytes) -> Slave:
        """
        依據 slave_id 拿到對應的 slave

        Args:
            slave_id (bytes): 目標的 slave id

        Returns:
            Slave: 與 slave id 對應的 Slave 物件
        """
        return next(filter(lambda slave: slave.get('slave_id') == slave_id, self.data), None)

    def set_heartbeat_data(self, slave_id: bytes, hearbeat_data: bytearray):
        """
        設定指定 slave 的 hearbeat data

        Args:
            slave_id (bytes): 該 slave 的 id
            hearbeat_data (bytearray): 要設定的 heartbeat data
        """
        new_hearbeat_info = {'heartbeat_data': hearbeat_data}
        self.__update_slave_info(slave_id, new_hearbeat_info)

    def set_response_time(self, slave_id: bytes, last_response_time: float):
        """
        設定指定 slave 的最後回應時間

        Args:
            slave_id (bytes): 該 slave 的 id
            last_response_time (float): 該 slave 最後回應的時間
        """
        new_response_time_info = {'last_response_time': last_response_time}
        self.__update_slave_info(slave_id, new_response_time_info)

    def get_recorder(self, slave_id: bytes) -> str:
        """
        回傳指定 slave 的 recorder 內容

        Args:
            slave_id (bytes): 該 slave 的 id

        Returns:
            str: 該 slave recorder 的內容
        """
        slave = self.get_slave(slave_id)
        slave_recorder = slave.get('status_recorde')
        return slave_recorder.get_json_string()

    def update_recorder(self, slave_id: bytes, new_recorder: dict):
        """
        更新目標 slave 的 recorder 內容，並寫入檔案

        Args:
            slave_id (bytes): 目標 slave 的 id
            new_recorder (dict): 要更新的內容
        """
        slave = self.get_slave(slave_id)
        slave_recorder = slave.get('status_recorde')
        slave_recorder.update(new_recorder)
        slave_recorder.write_to_file()

    def update_last_plug_status(self, slave_id: bytes, new_plug_status: str):
        new_plug_status_info = {'last_plug_status': new_plug_status}
        self.__update_slave_info(slave_id, new_plug_status_info)

    def get_last_plug_status(self, slave_id: bytes) -> str:
        slave = self.get_slave(slave_id)
        return slave.get('last_plug_status')

    def update_last_received_time(self, slave_id: bytes, last_received_time: float):
        new_last_received_time = {'last_received_time': last_received_time}
        self.__update_slave_info(slave_id, new_last_received_time)

    def get_last_received_time(self, slave_id: bytes) -> float:
        slave = self.get_slave(slave_id)
        return slave.get('last_received_time')

    def update_last_acc_power(self, slave_id: bytes, last_acc_power: int):
        new_last_acc_power = {'last_acc_power': last_acc_power}
        self.__update_slave_info(slave_id, new_last_acc_power)

    def get_last_acc_power(self, slave_id: bytes) -> int:
        slave = self.get_slave(slave_id)
        return slave.get('last_acc_power')

    def get_charger_id(self, slave_id: bytes) -> str:
        slave = self.get_slave(slave_id)
        return slave.get('charger_id')

    def get_vin_code(self, slave_id: bytes) -> str:
        """
        回傳目標 slave 的 vin code

        Args:
            slave_id (bytes): 目標的 slave id

        Returns:
            str: 解析完的 vin code
        """
        slave = self.get_slave(slave_id)
        vin_code_bytes = slave.get('vin_code')
        vin_code_hex_string = get_hex_string(vin_code_bytes)
        return bytes.fromhex(vin_code_hex_string).decode('utf-8')

    def set_vin_code(self, slave_id: bytes, vin_code: bytearray):
        """
        設定目標 slave 的 vin code

        Args:
            slave_id (bytes): 目標的 slave id
            vin_code (bytearray): 新的 vin code
        """
        slave = self.get_slave(slave_id)
        slave.update({'vin_code': vin_code})

    def clean_vin_code(self, slave_id: bytes):
        """
        清除掉目標 slave 的 vin code

        Args:
            slave_id (bytes): 目標的 slave id
        """
        slave = self.get_slave(slave_id)
        slave.update({'vin_code': bytearray()})

    def is_complete_vin_code(self, slave_id: bytes) -> bool:
        """
        檢查是否為合法的 vin code

        Args:
            slave_id (bytes): 目標的 slave id

        Returns:
            bool: 如果是合法的 vin code 回傳 True，反之回傳 False
        """
        dec_vin_code = self.get_vin_code(slave_id)
        if len(dec_vin_code) == 17:
            return True
        return False

    def __is_duplicate_slave_id(self, slave_id: str) -> bool:
        if not self.data:
            return False

        if self.get_slave(slave_id):
            return True
        return False

    def __update_slave_info(self, slave_id: bytes, new_info: dict):
        """
        更新目標 slave 的資料

        Args:
            slave_id (bytes): 要更新 slave 的 id
            new_info (dict): 新的資料
        """
        slave = self.get_slave(slave_id)
        slave.update(new_info)
