import sys
from os.path import abspath, dirname

root_folder = abspath(dirname(dirname(abspath(__file__))))
sys.path.append(root_folder)
