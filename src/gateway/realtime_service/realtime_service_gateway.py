#! /usr/bin/python3
# coding=utf-8

import threading
import time

import socketio
from socketio.exceptions import SocketIOError

from utils import is_thread_alive
from utils.logger import Logger
from utils.parser import XPClientCfgParser
from utils.scheduler import Scheduler

logger = Logger('realtime_service_gateway.log', level=Logger.DEBUG).get_logger()


class GatewayNamespace(socketio.ClientNamespace):
    """
    處理註冊 gateway 及何時傳送即時資料到 server 的任務
    """
    __xpclient_cfg_parser = None
    __sender_alive_time = None
    __scheduler = None

    def __init__(self, cfg_path='./xpclient.cfg', *args, **kwargs):
        self.__xpclient_cfg_parser = XPClientCfgParser(logger, cfg_path)
        self.__scheduler = Scheduler(logger=logger)
        super(GatewayNamespace, self).__init__(*args, **kwargs)

    def on_connect(self):
        logger.info('connected to server')
        self.register_gateway_id()

    def on_disconnect(self):
        logger.info('disconnect from server')
        self.__scheduler.stop_all_events()

    def on_set_sender_alive_time(self, alive_time: int):
        self.__sender_alive_time = alive_time

    def on_send_realtime_data(self):
        """
        嘗試啟動 sender，每五秒會送資料到 server，直到 requirer 要求停止或時限到期
        """
        if not is_thread_alive('sender'):
            gateway_id = self.__xpclient_cfg_parser.get_gateway_id()
            # TODO 實作拿電流資料
            forwarding_info = {'sender': gateway_id, 'data': 'test'}
            forwarding_event = (5, self.emit, ('forwarding_to_requirer', forwarding_info))
            self.__scheduler.add_event(forwarding_event)
            self.__scheduler.stop_scheduler_at(self.__sender_alive_time)

            logger.info('start sender thread')
            sender = threading.Thread(target=self.__scheduler.run, name='sender')
            sender.start()
        else:
            logger.debug('sender already running')

    def on_stop_sending_realtime_data(self):
        self.__scheduler.stop_all_events()

    def register_gateway_id(self):
        gateway_id = self.__xpclient_cfg_parser.get_gateway_id()
        logger.info('register gateway %s to server', gateway_id)
        self.emit('register_gateway', gateway_id)


class SocketioClient(object):
    __API_URL = ''
    __socketio_client = None

    def __init__(self, realtime_service: GatewayNamespace, api_url: str):
        self.__API_URL = api_url
        self.__socketio_client = socketio.Client()
        self.__socketio_client.register_namespace(realtime_service)

    def connect(self):
        self.__socketio_client.connect(self.__API_URL)
        self.__socketio_client.wait()

    def disconnect(self):
        self.__socketio_client.disconnect()

    def isconnected(self):
        return self.__socketio_client.connected


class RealtimeService(object):
    # TODO 改成由 api server 拿
    __API_URL = 'http://127.0.0.1:5005/'
    __realtime_service = GatewayNamespace()
    __socketio_client = SocketioClient(__realtime_service, __API_URL)

    def __init__(self):
        # TODO 對 api server 發送拿設定的請求
        pass

    def start(self):
        while not self.__socketio_client.isconnected():
            try:
                self.socketio_client.connect()
            except SocketIOError:
                logger.exception('can not connect to server: %s, re-connect to server after 60 seconds', self.__API_URL)
                time.sleep(60)


if __name__ == "__main__":
    realtime_service = RealtimeService()
    realtime_service.start()
