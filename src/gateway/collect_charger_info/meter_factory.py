from logging import RootLogger

from meter_reader import MeterPA3, MeterPA33


class MetersFactory(object):
    __logger = None
    __meters = ['MT_PA3', 'MT_PA33']

    def __init__(self, logger: RootLogger):
        self.__logger = logger

    def get_meter(self, meter_type: str, port: str):
        if meter_type not in self.__meters:
            self.__logger.error('not supported meter type: %s', meter_type)
            return None

        if meter_type == 'MT_PA3':
            return MeterPA3(port)
        elif meter_type == 'MT_PA33':
            return MeterPA33(port)
