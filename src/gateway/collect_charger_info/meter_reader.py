from abc import ABC
from typing import List

from pymodbus.client.sync import ModbusSerialClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder
from utils.logger import Logger

from meter_datas import MeterDatas

logger = Logger('meter_reader.log', level=Logger.DEBUG).get_logger()


class MetersReader(ABC):
    __data_size = {
        'int8': 1,
        'int16': 2,
        'uint16': 2,
        'int64': 8,
        'uint64': 8,
        'float32': 4
    }
    __modbus_client = None
    # 用於儲存回傳資料的 MeterDatas 物件
    _meter_datas = None
    # 要讀取的起始地址與要讀取的長度，可接受多值 e.g. [(0x1200, 30), (0x1100, 2)]
    _data_ranges = List[tuple]
    # 回傳資料的個別名稱，有順序性，可接受多值
    _fields_name = []
    # 回傳資料的資料型態
    _data_type = ''

    def __init__(self, port: str):
        self.__modbus_client = ModbusSerialClient(method='rtu', port=port, baudrate=9600, timeout=1)
        self.__modbus_client.connect()

    def read(self, modbus_id: int) -> MeterDatas:
        if not self.__are_properties_legal():
            raise ValueError('please check all properties are set up correctly')

        for data_range in self._data_ranges:
            start_address, length = data_range
            self.__read(modbus_id, start_address, length, self._data_type)
        return self._meter_datas

    def __are_properties_legal(self) -> bool:
        if not self._data_ranges or not self._fields_name or not self._data_type:
            return False
        return True

    def __read(self, modbus_id: int, start_address: int, length: int, data_type: str):
        count = self.__get_bytes_count(data_type, length)
        response = self.__modbus_client.read_input_registers(start_address, count, unit=modbus_id)

        if not response.isError():
            decoder = BinaryPayloadDecoder.fromRegisters(
                response.registers, byteorder=Endian.Big, wordorder=Endian.Little)
            self._meter_datas.update(self.__decode_data(length, decoder))
        else:
            logger.error('wrong response: %s', response)

    def __get_bytes_count(self, data_type: str, data_length: int) -> int:
        size = self.__data_size.get(data_type)
        count = size * data_length // 2
        if count == 0:
            count = 1
        return count

    def __decode_data(self, length: int, decoder: BinaryPayloadDecoder) -> dict:
        raw_meter_datas = []
        for _ in range(length):
            data = self.__decode_by_data_type(decoder)
            raw_meter_datas.append(data)

        meter_data = dict(zip(self._fields_name, raw_meter_datas))
        for field_name, data in list(meter_data.items()):
            if field_name == 'Reserved':
                meter_data.pop('Reserved')

        return meter_data

    def __decode_by_data_type(self, decoder: BinaryPayloadDecoder):
        if self._data_type == 'int8':
            return decoder.decode_8bit_int()
        elif self._data_type == 'int16':
            return decoder.decode_16bit_int()
        elif self._data_type == 'uint16':
            return decoder.decode_16bit_uint()
        elif self._data_type == 'int64':
            return decoder.decode_64bit_int()
        elif self._data_type == 'uint64':
            return decoder.decode_64bit_uint()
        elif self._data_type == 'float32':
            return decoder.decode_32bit_float()


class MeterPA3(MetersReader):
    _fields_name = ['Vln_a', 'Vln_b', 'Vln_c', 'Vln_avg', 'Vll_ab', 'Vll_bc', 'Vll_ca', 'Vll_avg',
                    'I_a', 'I_b', 'I_c', 'I_avg', 'Reserved', 'Freq',
                    'kW_a', 'kW_b', 'kW_c', 'kW_total', 'kvar_a', 'kvar_b', 'kvar_c', 'kvar_total', 'kVA_a', 'kVA_b', 'kVA_c', 'kVA_total',
                    'PF_a', 'PF_b', 'PF_c', 'PF_signed_avg',
                    'kWh_total', 'Reserved']
    _data_ranges = [(0x0400, 30), (0x049e, 2)]
    _data_type = 'float32'
    _meter_datas = MeterDatas('MT_PA3')

    def __init__(self, port: str):
        super().__init__(port)


class MeterPA33(MetersReader):
    _fields_name = ['V_a', 'I_a', 'kW_a', 'kvar_a', 'kVA_a', 'PF_a', 'kWh_a', 'kvarh_a', 'kVAh_a',
                    'V_b', 'I_b', 'kW_b', 'kvar_b', 'kVA_b', 'PF_b', 'kWh_b', 'kvarh_b', 'kVAh_b',
                    'V_c', 'I_c', 'kW_c', 'kvar_c', 'kVA_c', 'PF_c', 'kWh_c', 'kvarh_c', 'kVAh_c',
                    'V_avg', 'I_avg', 'kW_tot', 'kvar_tot', 'kVA_tot', 'PF_tot', 'kWh_tot', 'kvarh_tot', 'kVAh_tot']
    _data_ranges = [(0x1200, 36)]
    _data_type = 'int16'
    _meter_datas = MeterDatas('MT_PA33')

    def __init__(self, port: str):
        super().__init__(port)
