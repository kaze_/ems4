#! /usr/bin/python3
# coding=utf-8

import time
from queue import Queue

from utils.logger import Logger
from utils.scheduler import Scheduler

from meter_factory import MetersFactory

logger = Logger("collect_charger_info.log", level=Logger.DEBUG).get_logger()


class ChargerInfoCollector(object):
    """
    收集電錶及充電樁的電力資料
    """
    # [{'meter_name': 'MT_PA3', 'meter_data': MeterData()}]
    __meter_datas = []
    __meter_reader = None
    __meters_factory = MetersFactory(logger)
    # TODO 從設定檔拿到
    __meter_type = 'MT_PA33'
    __modbus_id = 1
    __meter_port = '/dev/ttymxc0'

    def start(self, commands_queue: Queue = None):
        scheduler = Scheduler(logger)
        collect_event = (1, self.__collect_meter_data, (self.__meter_type, self.__modbus_id))
        scheduler.add_event(collect_event)
        scheduler.run()

        while True:
            time.sleep(1)
            if commands_queue and not commands_queue.empty():
                command = commands_queue.get()
                logger.info('get command: %s', command)

    def __collect_meter_data(self, meter_type: str, modbus_id: int):
        self.__meter_reader = self.__meters_factory.get_meter(self.__meter_type, self.__meter_port)
        meter_data = self.__meter_reader.read(modbus_id)
        logger.debug('meter data: %s', meter_data)


if __name__ == '__main__':
    charger_info_collector = ChargerInfoCollector()
    charger_info_collector.start()
