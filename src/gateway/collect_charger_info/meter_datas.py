from collections import UserDict


class MeterDatas(UserDict):
    """
    紀錄每個電錶的回傳資料
    """
    meter_name = ''

    def __init__(self, meter_name: str):
        if not meter_name:
            raise ValueError

        self.meter_name = meter_name
        self.data = {}

    def __setitem__(self, key, value):
        self.data[key] = value
