#! /usr/bin/python3
# coding=utf-8

import threading
import time

from flask import Flask, request
from flask_socketio import Namespace, SocketIO

from utils import is_thread_alive
from utils.gateway_profile_manager import GatewayProfileManager
from utils.logger import Logger
from utils.redis_manager import RedisManager

app = Flask(__name__)
server_socketio = SocketIO(app)
logger = Logger('realtime_service_server.log', level=Logger.DEBUG).get_logger()


class ServerNamespace(Namespace):
    """
    紀錄 gateway 與 requirers 的關聯，並轉送即時資料到 requirer
    """
    __redis_manager = None
    __gateway_profile_manager = None
    __requirers_keeper = None
    # 設定 gateway 在 requirer 沒有發送結束請求時，還會持續送出多久的資料，單位秒
    __EXPIRE_TIME = 60 * 60

    def __init__(self, *args, **kwargs):
        self.__redis_manager = RedisManager(logger)
        self.__gateway_profile_manager = GatewayProfileManager(self.__redis_manager, logger)
        self.__requirers_keeper = RequirerInfoHandler(self.__gateway_profile_manager)
        super(ServerNamespace, self).__init__(*args, **kwargs)

    def on_disconnect(self):
        """
        斷線時嘗試清理斷線的 requirer 或 gateway_id，如果 gateway 沒有 requirer，則通知它停止傳輸
        """
        client_sid = request.sid
        client_host_info = request.headers.get(
            'X-Real-Ip') + ':' + request.headers.get('X-Real-Port')

        logger.info('client disconnect, sid: %s, host info: %s', client_sid, client_host_info)
        gateway_ids = self.__redis_manager.get_all_keys()
        for gateway_id in gateway_ids:
            gateway_sid = self.__gateway_profile_manager.get_gateway_sid(gateway_id)

            requirer = self.__gateway_profile_manager.find_requirer(client_host_info, gateway_id)
            if requirer:
                self.__gateway_profile_manager.remove_requirer(requirer, gateway_id)
                self.__try_stop_gateway_sending_data(gateway_id)
                break
            elif gateway_sid == client_sid:
                logger.info('remove gateway: %s', gateway_id)
                self.__redis_manager.remove(gateway_id)
                # TODO 需不需要讓 requirer 知道 gateway 斷線了
                break

    def on_register_gateway(self, gateway_id: str):
        """
        註冊 gateway

        Args:
            gateway_id (str): gateway id
        """
        old_gateway_info = self.__gateway_profile_manager.get_gateway_data(gateway_id)
        gateway_sid = request.sid
        gateway_info = None

        if old_gateway_info:
            requirers = self.__gateway_profile_manager.get_requirers(gateway_id)
            gateway_info = {'requirers': requirers, 'sid': gateway_sid}
            logger.info('gateway %s online angain, refresh gateway sid to %s',
                        gateway_id, gateway_sid)
        else:
            gateway_info = {'requirers': [], 'sid': gateway_sid}
            logger.info('register new gateway: %s', gateway_id)

        self.__redis_manager.write_from_json(gateway_id, gateway_info)
        self.emit('set_sender_alive_time', self.__EXPIRE_TIME)

    def on_require_realtime_data(self, gateway_id: str):
        """
        紀錄客戶端與目標 gateway 的關聯，並通知 gateway 開始發送資料到 server

        Args:
            gateway_id (str): 目標的 gateway id
        """
        # 從 header 讀是因為 nginx 轉送時不會帶真實 IP，只能設定 nginx 把資訊放在 header
        requirer_host_info = request.headers.get(
            'X-Real-Ip') + ':' + request.headers.get('X-Real-Port')
        requirer_sid = request.sid

        # 避免重複加入
        self.__gateway_profile_manager.remove_requirer_by_attribute(requirer_host_info, gateway_id)

        requirer_data = {'requirer_host_info': requirer_host_info,
                         'requirer_sid': requirer_sid, 'start_at': int(time.time())}
        self.__gateway_profile_manager.add_requirer(gateway_id, requirer_data)

        logger.info('notify gateway %s to start sending realtime data', gateway_id)
        gateway_sid = self.__gateway_profile_manager.get_gateway_sid(gateway_id)
        self.emit('send_realtime_data', room=gateway_sid)
        self.__launch_requirers_keeper(gateway_id)

    def on_forwarding_to_requirer(self, forwarding_info: dict):
        """
        轉送資料到與 gateway 有關連的客戶端

        Args:
            forwarding_info (dict): 包含 gateway_id 與要轉送的資料
        """
        gateway_id = forwarding_info.get('sender')
        gateway_requirers = self.__gateway_profile_manager.get_requirers(gateway_id)

        data = forwarding_info.get('data')
        for requirer in gateway_requirers:
            requirer_sid = requirer.get('requirer_sid')
            logger.info('forwarding data to %s', requirer.get('requirer_host_info'))
            self.emit('receive_realtime_data', data, room=requirer_sid)

    def on_require_stop_realtime_data(self, gateway_id: str):
        """
        停止轉送資料到與 gateway 關連的客戶端

        Args:
            gateway_id (str): 要停止接收的 gateway_id
        """
        requirer_host_info = request.headers.get(
            'X-Real-Ip') + ':' + request.headers.get('X-Real-Port')

        logger.info('stop translate from %s to %s', gateway_id, requirer_host_info)
        self.__gateway_profile_manager.remove_requirer_by_attribute(requirer_host_info, gateway_id)
        self.__try_stop_gateway_sending_data(gateway_id)

    def __launch_requirers_keeper(self, gateway_id: str):
        """
        每分鐘檢查與 gateway 關聯的 requirers 是否超過有效時限，直到所有的 requirer 都失效為止

        Args:
            gateway_id (str): 要監視的 gateway_id
        """
        thread_name = 'requirer_keeper_' + gateway_id
        if not is_thread_alive(thread_name):
            logger.info('start requirer_keeper thread')
            requirers_keeper = threading.Thread(
                target=self.__requirers_keeper.delete_unlegal_requirers, name=thread_name, args=(gateway_id, self.__EXPIRE_TIME))
            requirers_keeper.start()

    def __try_stop_gateway_sending_data(self, gateway_id: str):
        """
        當 gateway 沒有 requirer 時，通知它停止傳輸資料

        Args:
            gateway_id (str): 目標 gateway 的 id
        """
        gateway_requirers = self.__gateway_profile_manager.get_requirers(gateway_id)
        if not gateway_requirers:
            gateway_sid = self.__gateway_profile_manager.get_gateway_sid(gateway_id)
            logger.info('notify gateway %s to stop send data', gateway_id)
            self.emit('stop_sending_realtime_data', room=gateway_sid)


class RequirerInfoHandler(object):
    """
    維護 redis 裡的特定對象
    """
    __gateway_profile_manager = None

    def __init__(self, gateway_profile_manager: GatewayProfileManager):
        self.__gateway_profile_manager = gateway_profile_manager

    def delete_unlegal_requirers(self, gateway_id: str, alive_time: int):
        """
        每分鐘刪除超過時限的 requirer

        Args:
            gateway_id (str): 要監視的 gateway_id
            alive_time (int): 允許 requirer 的存活時間
        """
        while True:
            gateway_data = self.__gateway_profile_manager.get_gateway_data(gateway_id)
            if not gateway_data:
                break

            self.__clean_requirers(gateway_id, alive_time)
            gateway_requirers = self.__gateway_profile_manager.get_requirers(gateway_id)
            if not gateway_requirers:
                break
            time.sleep(60)

    def __clean_requirers(self, gateway_id: str, alive_time: int):
        gateway_requirers = self.__gateway_profile_manager.get_requirers(gateway_id)
        if not gateway_requirers:
            return

        now_time = int(time.time())
        for requirer in list(gateway_requirers):
            requirer_start_time = requirer.get('start_at')
            if now_time - requirer_start_time >= alive_time:
                logger.info('requirer %s over the alive time: %s seconds, delete it.',
                            requirer, alive_time)
                gateway_requirers.remove(requirer)
        self.__gateway_profile_manager.replace_requirer(gateway_id, gateway_requirers)


# TODO 啟動的時候需不需要清空 redis，防止有遺留的 requirers 沒刪除
if __name__ == "__main__":
    server_socketio.on_namespace(ServerNamespace())
    server_socketio.run(app, host='127.0.0.1', port=5005)
