import json
import re
from logging import RootLogger
from typing import Optional


class XPClientCfgParser(object):
    """
    解析 xpclient.cfg 的設定內容
    """
    __logger = None
    __cfg_path = ''
    __cfg_content = None

    def __init__(self, logger: RootLogger, cfg_path='./xpclient.cfg'):
        if not logger:
            raise ValueError
        self.__logger = logger
        self.__cfg_path = cfg_path
        self.__cfg_content = self.__read_cfg(cfg_path)

    def get_cfg_as_json(self) -> dict:
        try:
            return json.loads(self.__cfg_content)
        except json.JSONDecodeError as err:
            self.__logger.error('can not convert to json: %s', err)

    def get_gateway_id(self) -> str:
        cfg_content = self.get_cfg_as_json()
        gateway_id = cfg_content.get('modbus')[0].get('id')
        return gateway_id

    def get_circuit_ids(self) -> list:
        """
        從設定檔得到所有的 circuit id
        """
        cfg_json = self.get_cfg_as_json()
        circuit_ids = []
        try:
            # TODO 這支只放在 fat gateway，有可能會因為場域的 cfg 格式不同，讀取的欄位會不同
            # circuits = cfg.get('modbus')[0].get('powerMeters')[0].get('circuits')[0].get('children')
            circuits = cfg_json.get('modbus')[0].get('powerMeters')[0].get('circuits')
            for current_circuit in circuits:
                circuit_id = current_circuit.get('id')
                circuit_ids.append(circuit_id)
        except TypeError as err:
            self.__logger.error('An error occurred while visiting the cfg: %s', err)
        return circuit_ids

    def __read_cfg(self, path):
        try:
            with open(path, 'rb') as cfg_file:
                binary_data = cfg_file.readlines()
                binary_data = ''.join(map(str, binary_data))
                # 只處理 devices 的部份，gateway structure 不處理
                json_data = re.search(r'{.+[^{]]}[^{"children"]}', str(binary_data)).group(0)
                # 因 xpclient 寫入的檔案會有亂碼，所以要先做處理
                json_data = re.sub(r'\\x..', r'', json_data)
                return json_data
        except Exception as err:
            self.__logger.error('Can not read %s, error: %s', self.__cfg_path, err)
            return None


class EnvParser(object):
    """
    解析 .env 檔的內容
    """
    __logger = None

    def __init__(self, logger: RootLogger):
        self.__logger = logger

    @classmethod
    def load_env(cls, env_path='/home/xmight/Desktop/storage0/.env', logger: RootLogger = None) -> Optional[dict]:
        """
        解析 .env 檔中的內容

        Args:
            env_path (str, optional): .env 檔的路徑. Defaults to '/home/xmight/Desktop/storage0/.env'.

        Returns:
            dict/None: 解析成功則回傳 key/value 回去，否則回傳 None
        """
        if logger is None:
            raise ValueError

        if cls.__logger is None:
            __logger = logger
        else:
            __logger = cls.__logger

        lines = []
        try:
            with open(env_path) as env_file:
                lines = env_file.readlines()
        except FileNotFoundError:
            __logger.error('can not found env file: %s', env_path)
            return None

        env = {}
        for line in lines:
            line = line.strip()
            if line.startswith('#'):
                continue

            tokens = line.split('=')
            if len(tokens) > 1:
                key = tokens[0]
                value = tokens[1]
                env.update({key: value})
            else:
                __logger.error('can not parse .env file, wrong format')
        return env
