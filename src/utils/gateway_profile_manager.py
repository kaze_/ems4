from logging import RootLogger
from typing import List

from utils.redis_manager import RedisManager


class GatewayProfileManager(object):
    """
    用於管理 gateway profile 的一些 utils
    """
    __redis_manager = None
    __logger = None

    def __init__(self, redis_manager: RedisManager, logger: RootLogger):
        self.__redis_manager = redis_manager
        self.__logger = logger

    def find_requirer(self, requirer_attr: str, gateway_id: str) -> dict:
        """
        依據 requirer 的各個屬性去尋找目標

        Args:
            requirer_attr (str): 目標 requirer 的任一屬性
            gateway_id (str): 要在哪個 gateway 尋找

        Returns:
            dict: 找到的 requirer，找不到則回傳 null dict
        """
        gateway_data = self.get_gateway_data(gateway_id)
        if not gateway_data:
            return {}

        requirers = gateway_data.get('requirers')
        return next(filter(lambda requirer: requirer_attr in requirer.values(), requirers), None)

    def remove_requirer(self, requirer: dict, gateway_id: str):
        """
        直接刪除 requirer，不檢查是否存在 gateway profile 裡

        Args:
            requirer (dict): 要刪除的 requirer
            gateway_id (str): requirer 所在的 gateway
        """
        self.__logger.info('remove requirer: %s', requirer)
        requirers = self.get_requirers(gateway_id)
        requirers.remove(requirer)
        self.replace_requirer(gateway_id, requirers)

    def remove_requirer_by_attribute(self, requirer_attr: str, gateway_id: str):
        """
        刪除與 gateway 關聯的 requirer，並更新到 redis

        Args:
            requirer_attr (str): 要刪除的 requirer 具有的任一屬性
            requires (dict): 所有的 requirers
        """
        requirer = self.find_requirer(requirer_attr, gateway_id)
        if not requirer:
            return

        self.remove_requirer(requirer, gateway_id)

    def add_requirer(self, gateway_id: str, new_requirer: dict):
        """
        新增 requirer 到該 gateway，並寫入 redis

        Args:
            gateway_id (str): 要新增哪個 gateway 的 requirer
            new_requirer (dict): 要新增的 requirer
        """
        gateway_data = self.get_gateway_data(gateway_id)
        if not gateway_data:
            return

        requirers = gateway_data.get('requirers')
        requirers.append(new_requirer)
        self.__logger.info('add new requirer %s to gateway: %s', new_requirer, gateway_id)
        self.__redis_manager.write_from_json(gateway_id, gateway_data)

    def get_requirers(self, gateway_id: str) -> List[dict]:
        """
        回傳目標 gateway 的所有 requirers

        Args:
            gateway_id (str): 目標的 gateway id

        Returns:
            list: 找到的話回傳所有 requirers 的 list，找不到則回傳 null list
        """
        gateway_data = self.get_gateway_data(gateway_id)
        if not gateway_data:
            return []

        return gateway_data.get('requirers')

    def get_gateway_sid(self, gateway_id: str) -> str:
        """
        回傳目標 gateway 的 sid

        Args:
            gateway_id (str)): 目標的 gateway id

        Returns:
            str: 找到的話回傳 sid，找不到則回傳 null str
        """
        gateway_data = self.get_gateway_data(gateway_id)
        if not gateway_data:
            return ''

        return gateway_data.get('sid')

    def get_gateway_data(self, gateway_id: str) -> dict:
        """
        依據 gateway id 取得對應的值

        Args:
            gateway_id (str): 目標的 gateway id

        Returns:
            dict: gateway 對應的 profile
        """
        gateway_data = self.__redis_manager.read_as_json(gateway_id)
        if gateway_data is None:
            self.__logger.error('gateway id %s not exist', gateway_id)
        elif not gateway_data:
            self.__logger.error('profile of gateway %s is empty', gateway_id)

        return gateway_data

    def replace_requirer(self, gateway_id: str, new_requirers: List[dict]):
        """
        取代目標 gateway 的 requirer 成新的值，並寫入 redis

        Args:
            gateway_id (str): 要取代哪個 gateway 的 requirers
            new_requirers (list): 新的 requirers
        """
        gateway_data = self.get_gateway_data(gateway_id)
        old_requirers = self.get_requirers(gateway_id)
        if not gateway_data or self.__equal_requirers(old_requirers, new_requirers):
            return

        gateway_data.update({'requirers': new_requirers})
        self.__redis_manager.write_from_json(gateway_id, gateway_data)

    def clean_all_gateway_profiles(self):
        """
        刪除所有在 redis 裡的 gateway profiles
        """
        gateway_ids = self.__redis_manager.get_all_keys()
        for gateway_id in gateway_ids:
            self.__redis_manager.remove(gateway_id)
        self.__logger.info('clean all old gateway profiles')

    def __equal_requirers(self, old_requirers: List[dict], new_requirers: List[dict]) -> bool:
        """
        比較兩組 requirers 中的成員是否都相同

        Args:
            old_requirers (list): 要比較的 requirers
            new_requirers (list): 要比較的 requirers

        Returns:
            bool: 兩組中的成員都相同則回傳 True，反之回傳 False
        """
        for old_requirer in old_requirers:
            if old_requirer not in new_requirers:
                return False
        return True
