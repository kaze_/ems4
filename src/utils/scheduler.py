import sched
import time
from logging import RootLogger
from typing import Any, Callable, List, Tuple

from utils.date import get_isoformat_datetime


class Scheduler(object):
    """
    定時執行任務
    """
    __logger = None
    __scheduler = None
    __time_for_stop_scheduler = 0
    __events = []
    __delay_interval = 0

    def __init__(self, logger: RootLogger, event: Tuple[int, Callable[..., Any], Tuple[Any, ...]] = ()):
        if not logger:
            raise ValueError
        self.__logger = logger

        if event:
            self.add_event(event)

    def run(self):
        """
        開始定時執行任務
        """
        self.__scheduler = sched.scheduler(time.time, time.sleep)

        while self.__scheduler.empty() and self.__events:
            try:
                now_time = int(time.time())
                if self.__time_for_stop_scheduler and now_time >= self.__time_for_stop_scheduler:
                    self.__time_for_stop_scheduler = 0
                    break

                self.__set_events_to_scheduler()
                next_run_time = now_time + self.__delay_interval
                self.__logger.info('the next execute time: %s', get_isoformat_datetime(next_run_time))

                self.__scheduler.run()
            except Exception as err:
                self.__logger.exception('scheduled event occurs exception: %s', err)
                time.sleep(60)

            if self.__get_not_finished_events(self.__scheduler.queue):
                self.stop_all_events()

    def stop_scheduler_at(self, alive_time: int):
        """
        設定 scheduler 從現在開始後多久結束（秒）

        Args:
            alive_time (int): scheduler 的存活時間（秒）
        """
        if not isinstance(alive_time, int) and not alive_time >= 1:
            return

        stop_running_at = int(time.time()) + alive_time
        self.__logger.info('set scheduler expire time at : %s', get_isoformat_datetime(stop_running_at))
        self.__time_for_stop_scheduler = stop_running_at

    def stop(self, target_event: sched.Event):
        target_event_info = next(filter(lambda event_info: target_event == event_info.get('event'), self.__events))
        if target_event_info:
            self.__scheduler.cancel(target_event)
            self.__events.remove(target_event_info)
        else:
            self.__logger.error('can not stop not exist event: %s', target_event)

    def stop_all_events(self):
        """
        停止所有的正在運行的 events
        """
        if not self.__scheduler:
            self.__logger.error('can not stop not events before scheduler running')
            return

        self.__time_for_stop_scheduler = int(time.time())
        for event in self.__scheduler.queue:
            try:
                self.stop(event)
            except ValueError as err:
                self.__logger.error('can not cancel not running event: %s, error: %s', event, err.msg)

    def get_events(self) -> List[sched.Event]:
        events = []
        for event_info in self.__events:
            event = event_info.get('event')
            events.append(event)
        return events

    def add_event(self, event: Tuple[int, Callable[..., Any], Tuple[Any, ...]]):
        """
        增加新的 event 到 scheduler 裡

        Args:
            event (tuple): 新事件的各個參數，參數有順序性：(delay_interval, callable_function, args)

        Raises:
            ValueError: 如果要加入的事件不符合條件，則拋出錯誤
        """
        if self.__validate_event(event):
            self.__logger.debug('add new event: %s', event)
            events_info = {'event_args': event, 'event': None}
            self.__events.append(events_info)
        else:
            self.__logger.error('wrong event format: %s', event)
            raise ValueError

    def __set_events_to_scheduler(self):
        if not self.__events:
            return

        for event_info in self.__events:
            delay_interval, callable_function, args = event_info.get('event_args')
            self.__delay_interval = delay_interval
            event = self.__scheduler.enter(delay_interval, 0, callable_function, args)
            event_info.update({'event': event})

    def __get_not_finished_events(self, events_queue: list) -> list:
        events = self.get_events()
        not_finished_events = list(filter(lambda event: event in events_queue, events))

        self.__logger.debug('not finished events: %s', not_finished_events)
        return not_finished_events

    def __validate_event(self, event: tuple) -> bool:
        """
        檢查 event 有沒有符合條件：(delay_interval, callable_function, args)
        delay_interval: 一秒以上，
        callable_function: 可呼叫的函數，
        args: 以 tuple 類型附加

        Args:
            event (tuple): 要加入的 event

        Returns:
            bool: 符合條件則回傳 True，反之回傳 False
        """
        if not event or len(event) != 3:
            return False

        delay_interval = event[0]
        callable_function = event[1]
        args = event[2]
        return callable(callable_function) and delay_interval > 0 and isinstance(args, tuple)
