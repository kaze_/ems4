from datetime import datetime
from typing import Union


def get_isoformat_datetime(timestamp: Union[int, float]) -> str:
    """
    把 timestamp 轉換成 ISO-8601 格式

    Args:
        timestamp (int/float): 要轉換的 timestamp

    Returns:
        str: ISO-8601 格式的時間字串
    """
    if not timestamp or not isinstance(timestamp, (int, float)):
        return ''

    try:
        new_datetime = datetime.fromtimestamp(timestamp)
    except OverflowError:
        return ''
    return datetime.isoformat(new_datetime)
