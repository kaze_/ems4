import threading
from typing import Union


def is_thread_alive(thread_name: str) -> bool:
    """
    確認目標 thread 是否還在執行

    Args:
        thread_name (str): 要查詢的 thread 名稱

    Returns:
        bool: 還在執行回傳 True，反之回傳 False
    """
    all_threads_name = list(
        map(lambda current_thread: current_thread.name, threading.enumerate()))

    if thread_name in all_threads_name:
        return True
    return False


def get_hex_string(source_btyearray: Union[bytearray, bytes, str]) -> str:
    if not source_btyearray or not isinstance(source_btyearray, (bytearray, bytes, str)):
        return ''

    if isinstance(source_btyearray, str):
        source_btyearray = source_btyearray.encode('utf-8')
    return ' '.join('{:02X}'.format(current_byte) for current_byte in source_btyearray)
