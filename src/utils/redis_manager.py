import json
import time
from logging import RootLogger
from typing import Optional, Union

import redis

from utils.date import get_isoformat_datetime


class RedisManager(object):
    """
    管理與 redis 的 I/O，Connection
    """
    __logger = None
    __redis_db = None

    def __init__(self, logger: RootLogger, host='127.0.0.1', port=6379):
        if not logger:
            raise ValueError
        self.__logger = logger

        try:
            self.__redis_db = redis.Redis(host, port, db=0, decode_responses=True)
        except redis.exceptions.ConnectionError:
            logger.error('can not connect to redis server: %s:%s', host, port)

    def write(self, name: str, data: Union[dict, str]) -> bool:
        """
        把資料寫到 redis 裡

        Args:
            name (str): 要寫入的 key name
            data (str/dict): 要寫入的值，目前支援 string 與 hash

        Returns:
            boolean: 寫入成功則回傳 True，不成功回傳 False
        """
        if isinstance(data, str):
            return self.__write_string(name, data)
        elif isinstance(data, dict):
            return self.__write_dict(name, data)
        else:
            self.__logger.error('not support %s data type', type(data))

    def write_from_json(self, name: str, data: Union[dict, list]) -> bool:
        """
        把 json 對應的物件轉成字串再寫入 redis

        Args:
            name (str): 要寫入的 key name
            data (dict/list): 要寫入的 JSON 物件
        Returns:
            boolean: 寫入成功則回傳 True，不成功回傳 False
        """
        try:
            json_string = json.dumps(data)
        except json.JSONDecodeError as err:
            self.__logger.error(
                'can not convert %s to JSON string, error message: %s', data, err.msg)
        finally:
            self.write(name, json_string)

    def read(self, gateway_id: str) -> Union[str, dict, None]:
        """
        從 redis 讀取值

        Args:
            gateway_id (str): 要讀取內容的 key name

        Returns:
            str/dict/None: 回傳目標的值，有錯誤發生則回傳 None
        """
        value_type = self.__redis_db.type(gateway_id)
        if value_type == 'string':
            return self.__redis_db.get(gateway_id)
        elif value_type == 'hash':
            return self.__redis_db.hgetall(gateway_id)
        elif value_type == 'none':
            return None
        else:
            self.__logger.error('not support %s data type', value_type)
            return None

    def read_as_json(self, gateway_id: str) -> Optional[dict]:
        """
        從 redis 讀取值，並轉成 JSON 物件

        Args:
            gateway_id (str): 要讀取內容的 key name
        Returns:
            dict/None: 回傳與 JSON 對應的物件，如果 gateway id 不存在，則回傳 None
        """
        gateway_data = self.read(gateway_id)
        if gateway_data:
            return json.loads(gateway_data)
        elif gateway_data is None:
            return None
        return {}

    def remove(self, gateway_id: str):
        self.__redis_db.delete(gateway_id)

    def get_expire_time(self, gateway_id: str) -> int:
        """
        回傳特定 gateway_id 的到期時間（秒）

        Returns:
            int: key 到期的時間，如果有錯誤，則回傳 -1
        """
        key_alive_time = self.__redis_db.ttl(gateway_id)
        if key_alive_time >= 0:
            expire_time = int(time.time()) + key_alive_time
            return expire_time
        else:
            self.__logger.error('can not find alive time of %s', gateway_id)
            return -1

    def close(self):
        self.__redis_db.close()

    def set_expire_time(self, name: str, interval=60 * 60):
        """
        對目標設定過期時間，預設是一小時

        Args:
            name (str): 要設定過期時間的 key name
            interval (int) 幾秒後過期
        """
        expire_time = int(time.time()) + interval
        self.__redis_db.expireat(name, expire_time)
        self.__logger.info('set %s expire_time: %s', name, get_isoformat_datetime(expire_time))

    def get_all_keys(self) -> list:
        cursor, keys = self.__redis_db.scan(0)
        return keys

    def __write_dict(self, name: str, values: dict):
        if not isinstance(values, dict):
            self.__logger.error('not supported data type: %s, except for dict', type(values))
            return

        try:
            self.__redis_db.hmset(name, values)
            self.__logger.info('write %s: %s to redis', name, values)
        except (redis.RedisError, redis.ConnectionError) as err:
            self.__logger.exception(
                'can not write data to redis, data: %s, \nerror: %s', values, err)

    def __write_string(self, name: str, value: str):
        if not isinstance(value, str):
            self.__logger.error('not supported data type: %s, except for string', type(value))
            return

        try:
            self.__redis_db.set(name, value)
            self.__logger.info('write %s: %s to redis', name, value)
        except (redis.RedisError, redis.ConnectionError) as err:
            self.__logger.exception(
                'can not write data to redis, data: %s, \nerror: %s', value, err)
