import logging
from itertools import chain


class LoggerManager(object):
    __instance = None
    __loggers = []

    def __new__(cls, *args, **kwargs):
        if cls.__instance is None:
            cls.__instance = super().__new__(cls)
        return cls.__instance

    def __init__(self, logger: logging.RootLogger):
        self.__loggers.append(logger)

    @classmethod
    def get_loggers(cls) -> list:
        return cls.__loggers

    @classmethod
    def get_file_handlers(cls) -> list:
        """
        回傳所有 loggers 的 FileHandler，只適用於 utils 的 Logger 模組

        Returns:
            list: 所有 loggers 的 FileHandler
        """
        all_handlers = list(chain.from_iterable(map(lambda logger: logger.handlers, cls.get_loggers())))
        file_handlers = list(filter(lambda handler: isinstance(handler, logging.FileHandler), all_handlers))
        return file_handlers


class Logger(LoggerManager):
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL

    DEFAULT_FORMATTER = '[%(levelname)s %(asctime)s %(name)s %(module)s:%(lineno)d] %(message)s'

    __logger = None
    __logger_name = ''

    def __init__(self, name, formatter=DEFAULT_FORMATTER, level=INFO):
        self.__logger_name = name
        self.__logger = logging.getLogger(name)

        config = {'level': level, 'formatter': formatter}
        self.__init_config(config)
        super().__init__(self.__logger)

    def get_logger(self) -> logging.RootLogger:
        return self.__logger

    def __init_config(self, config):
        formatter = logging.Formatter(config.get('formatter'))
        level = config.get('level')

        file_handler = logging.FileHandler(self.__logger_name)
        file_handler.setFormatter(formatter)
        self.__logger.addHandler(file_handler)

        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        self.__logger.addHandler(console_handler)

        self.__logger.setLevel(level)
