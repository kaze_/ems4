#!/bin/bash

build_path='./build'
if [[ ! -d $build_path ]]; then
    mkdir $build_path
fi
cd $build_path

build_target="$1"
package_name=''
builder() {
#    package_name="$build_target.tar.gz"
    if [[ -f $package_name ]]; then
        rm $package_name
    fi
    mkdir ./$build_target

    if [[ -d ../gateway/$build_target ]]; then
        cp -r ../gateway/$build_target ./$build_target/gateway
        cp -r ../utils ./$build_target/gateway/
    fi
    if [[ -d ../server/$build_target ]]; then
        cp -r ../server/$build_target ./$build_target/server
        cp -r ../utils ./$build_target/server/
    fi
    tar zcvf $package_name $build_target/*
    rm -rf ./$build_target
    copy_to
}

deploy() {
    if [[ -f $package_name ]]; then
        rm $package_name
    fi
    mkdir -p ./XMT-EMS4/gateway/$build_target
    mkdir -p ./XMT-EMS4/server/$build_target

    if [[ -d ../gateway/$build_target ]]; then
        cp -r ../gateway/$build_target/* ./XMT-EMS4/gateway/$build_target/
        cp -r ../utils ./XMT-EMS4/gateway/
    fi
    cp ../gateway/gateway_daemon/gateway_daemon.py ./XMT-EMS4/gateway/
    cp ../gateway/gateway_daemon/run_gateway_daemon.sh ./XMT-EMS4/gateway/

    if [[ -d ../server/$build_target ]]; then
        cp -r ../server/$build_target/* ./XMT-EMS4/server/$build_target/
        cp -r ../utils ./XMT-EMS4/server/
    fi
    # TODO 複製 server 端的 daemon
}

copy_to() {
    ans=''
    read -p '需要複製到目標機器嗎？[y/n]' ans
    if [[ $ans == 'y' ]]; then
        start_ip=''
        end_ip=''
        read -p 'Please Enter Start IP ' start_ip
        read -p 'Please Enter End IP ' end_ip
        start_ip=$(echo $start_ip | cut -d '.' -f 4)
        end_ip=$(echo $end_ip | cut -d '.' -f 4)

        target_path=''
        read -p 'Enter target path ' target_path
        target_path=${target_path%/}

        for i in $(seq $start_ip $end_ip); do
            echo 'copy from 10.8.0.$i:'
            scp -o ConnectTimeout=10 -P 9980 ./$package_name xmight@10.8.0.$i:$target_path/
        done
    fi
}

program_names=('realtime_service' 'collect_charger_info' 'TWCManager')
if [[ $build_target == 'all' ]]; then
    for((i=0; i < ${#program_names[@]}; i++)); do
        build_target=${program_names[i]}
        package_name="$build_target.tar.gz"
        builder
    done
elif [[ $build_target == 'deploy' ]]; then
    package_name="XMT-EMS4.tar.gz"
    for((i=0; i < ${#program_names[@]}; i++)); do
        build_target=${program_names[i]}
        deploy
    done
    tar zcvf $package_name XMT-EMS4/*
    rm -rf ./XMT-EMS4
    copy_to
else
    for((i=0; i < ${#program_names[@]}; i++)); do
        if [[ $build_target == ${program_names[i]} ]]; then
            package_name="$build_target.tar.gz"
            builder
        fi
    done
fi
