#!/bin/bash

pip install poetry

if [[ "$1" == "deploy"]]; then
    poetry install --no-dev
elif [[ "$1" == "dev" ]]; then
    poetry install --no-root
elif [[ "$1" == "all" ]]; then
    poetry install
fi
